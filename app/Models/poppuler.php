<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poppuler extends Model
{
    use HasFactory;
    protected $table = 'poppuler';
    protected$fillable = ['gambar','nama', 'harga'];
}