<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class varianrasa extends Model
{
    use HasFactory;
    protected $table = 'varianrasa';
    protected$fillable = ['gambar','nama', 'harga'];
}