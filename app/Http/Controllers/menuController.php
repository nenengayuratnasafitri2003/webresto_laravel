<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\varianrasa;

class menuController extends Controller
{
     public function index(Request $request)
    {
        if($request->has('varianrasa')){
            $menu = Menu::where('nama', '%'.$request->varianrasa.'%')->get();
        } else {
            $menus = varianrasa::all();
        }
        
        return view('crud.index', ['menus'=>$menus]);
    }

    public function create(Request $request)
    {
        $menus = varianrasa::create([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/menus')->with('success', 'Data Berhasil Diinput');
    }

    public function edit($id)
    {
        $menus = varianrasa::find($id);
        return view('/crud/edit', ['menus'=>$menus]);
    }

    public function update(Request $request, $id)
    {
        $menus = varianrasa::where('id',$id)->update([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/menus')->with('success', 'Data Berhasil diupdate');
    }

    public function delete($id)
    {
        $menus = varianrasa::find($id);
        $menus->delete();

        return redirect('/menus')->with('success', 'Data Berhasil dihapus');
    }
}