<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


// to view
Route::get('/poppuler', function () {
    return view('home');
});
Route::get('/rasa', function(){
    return view('varian');
});
Route::get('/menu', function(){
    return view('menu');
});
Route::get('/topping', function(){
    return view('topping');
});
Route::get('/promo', function(){
    return view('promo');
});

Route::get('/menus', 'menuController@index');
Route::post('/menus/create', 'menuController@create');
Route::get('/menus/{id}/edit', 'menuController@edit');
Route::post('/menus/{id}/update', 'menuController@update');
Route::get('/menus/{id}/delete', 'menuController@delete');
