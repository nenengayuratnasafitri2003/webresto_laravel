<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\rasaController;
use App\Http\Controllers\promoController;
use App\Http\Controllers\toppingController;
use App\Http\Controllers\poppulerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getrasa', [varianrasaController::class, 'index']); 
Route::get('/getpromo', [promoController::class, 'index']); 
Route::get('/gettopping', [toppingController::class, 'index']); 
Route::get('/getpoppuler', [poppulerController::class, 'index']);