var varian = [
  {
    "gambar" : "assets/img/varian/matcha.png",
    "nama" : "Matcha Tea Latte",
    "harga" : "24000"
  },
  {"gambar" : "assets/img/varian/oolong.png",
    "nama" : "Oolong Tea Latte",
    "harga" : "24000"
  },
  {
    "gambar" : "assets/img/varian/jasmine.png",
    "nama" : "Jasmine Green Milk Tea",
    "harga" : "22000"
  },
  {
    "gambar" : "assets/img/varian/straw.png",
    "nama" : "Strawberry Milk Tea",
    "harga" : "23000"
  },
  {
    "gambar" : "assets/img/varian/Honey.png",
    "nama" : "Honey Milk Tea",
    "harga" : "23000"
  },
  {
    "gambar" : "assets/img/varian/Fruity.png",
    "nama" : "Passion Fruit Green Milk Tea",
    "harga" : "20000"
  },
  {
    "gambar" : "assets/img/varian/winter.png",
    "nama" : "Wintermelon Tea Latte",
    "harga" : "23000"
  },
{
    "gambar" : "assets/img/varian/red.png",
    "nama" : "Matcha Red Bean",
    "harga" : "25000"
  },
  {
    "gambar" : "assets/img/varian/taro.png",
    "nama" : "Taro Milk Tea",
    "harga" : "25000"
  },
  {
    "gambar" : "assets/img/varian/cocoa.png",
    "nama" : "Pure Cocoa",
    "harga" : "25000"
  },
  {
    "gambar" : "assets/img/varian/time.png",
    "nama" : "Chatime Milk Tea",
    "harga" : "23000"
  },
  {
    "gambar" : "assets/img/varian/berry.png",
    "nama" : "Strawberry Smoothie",
    "harga" : "21000"
  },
  {
    "gambar" : "assets/img/varian/tea.png",
    "nama" : "Thai tea Original",
    "harga" : "24000"
  },
  {
    "gambar" : "assets/img/varian/apple.png",
    "nama" : "Apple Green Tea",
    "harga" : "23000"
  },
  {
    "gambar" : "assets/img/varian/sugar.png",
    "nama" : "Brown Sugar Classic Tea Latte",
    "harga" : "25000"
  },
  {
    "gambar" : "assets/img/varian/fresh.png",
    "nama" : "Grass Jelly with Fresh Milk",
    "harga" : "21000"
  },
  {
    "gambar" : "assets/img/varian/mango.png",
    "nama" : "Mango Green Milk Tea",
    "harga" : "23000"
  },
  {
    "gambar" : "assets/img/varian/hazelnut.png",
    "nama" : "Hazelnut Chocolate Milk Tea",
    "harga" : "25000"    
  }
];
const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');
    fetch('api/getrasa')

    elmnt.innerHTML +=` <div class="row" style="float: right;">
                                    <div class="col mb-3" >
                                    <div class="card m-2" style="width: 15rem;">
                                <img src="${item.gambar}" class="card-img-top" alt="...">
                                <div class="card-body bg-light">
                                  <h5 class="card-title text-dark">${item.nama}</h5>
                                  <p class="text-dark">Rp.${item.harga}</p>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i><br>
                                  <a href="#" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                              </div>
                            </div>
                            </div>
                    </div>`

}
                    varian.map(callbackMap);
                    jumlahMENU(varian);
                    
                    const buttonElmnt = document.querySelector('.button-cari');
                    
                    buttonElmnt.addEventListener('click', () => {
                    
                      const hasilPencarian = varian.filter((item, index) => {
                                            const inputElmnt = document.querySelector('.input-keyword');
                                            const namaItem = item.nama.toLowerCase();
                                            const keyword = inputElmnt.value.toLowerCase();
                    
                                            return namaItem.includes(keyword);
                              })
                    
document.querySelector('#search').innerHTML ='';
hasilPencarian.map(callbackMap);
                    });