var bubble = [
  
  {
    "gambar" : "assets/img/topping/coffe.png",
    "nama" : "Coffe Jelly",
    "harga" : "5.000"
  },
  {
    "gambar" : "assets/img/topping/grass.png",
    "nama" : "Grass Jelly",
    "harga" : "4.000"
  },
  {
    "gambar" : "assets/img/topping/jelly.png",
    "nama" : "Rainbow Jelly",
    "harga" : "5.000"
  },
  {
    "gambar" : "assets/img/topping/pudding.png",
    "nama" : "Pudding",
    "harga" : "4.000"
  },
  {
    "gambar" : "assets/img/topping/aloe.png",
    "nama" : "Aloe vera",
    "harga" : "5.000" 
  },
  {
    "gambar" : "assets/img/topping/red bean.png",
    "nama" : "Red bean",
    "harga" : "5.000" 
  },
  {
    "gambar" : "assets/img/topping/bubble.png",
    "nama" : "Pearls",
    "harga" : "4.000"
  },
  {
    "gambar" : "assets/img/topping/coco.png",
    "nama" : "Coconut Jelly",
    "harga" : "5.000"
  }
];


const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');
    fetch('api/gettopping')

    elmnt.innerHTML += `<div class="row" style="float: left;">
                        <div class="col mb-3" >
                        <div class="card m-3" style="width: 19rem;">
                    <img src="${item.gambar}" class="card-img-top" alt="...">
                    <div class="card-body bg-light">
                      <h5 class="card-title text-dark">${item.nama}</h5>
                      <p class="text-dark">Rp.${item.harga}</p>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i><br>
                      <a href="#" class="btn btn-primary">Beli Sekarang</a>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>`
}

bubble.map(callbackMap);
jumlahMENU(bubble);

const buttonElmnt = document.querySelector('.button-cari');

buttonElmnt.addEventListener('click', () => {

  const hasilPencarian = bubble.filter((item, index) => {
                        const inputElmnt = document.querySelector('.input-keyword');
                        const namaItem = item.nama.toLowerCase();
                        const keyword = inputElmnt.value.toLowerCase();

                        return namaItem.includes(keyword);
          })

          document.querySelector('#search').innerHTML ='';
          hasilPencarian.map(callbackMap);
});