var promo = [
  {
    "gambar" : "assets/img/promo/promo1.jpg",
    "desc" : "Chatime Promo Spesial Gofood, 3 Milk Tea Series Hanya Rp 50.000!",
    "berlaku" : "Berlaku hingga : 29 September 2020"
  },
  {
    "gambar" : "assets/img/promo/promo2.jpg",
    "desc" : "Chatime Promo Popcan To Go, Mulai dari Rp 24 Ribuan! Dapatkan segera!",
    "berlaku" : "Berlaku hingga : 30 September 2020"
  },
  {
    "gambar" : "assets/img/promo/promo3.jpeg",
    "desc" : "Chatime Promo Happy Hour, Tambah Rp. 1.000 Dapat 2 Sekaligus!",
    "berlaku" : "Berlaku hingga : 30 September 2020"
  },
  {
    "gambar" : "assets/img/promo/promo4.jpeg",
    "desc" : "Chatime Promo Spesial MERDEKA, Diskon 25%! Jangan sampai kehabisan.",
    "berlaku" : "Berlaku hingga : 29 September 2020"
  }
];

promo.map((data, key) => {
  let murah = ` <div class="row" style="float: left;">
                      <div class="col mb-8" >
                        <div class="card m-3" style="width: 19rem;">
                                <img src="${data.gambar}" class="card-img-top" alt="...">
                                <div class="card-body bg-light">
                                  <h5 class="card-title text-dark">${data.desc}</h5>
                                  <p class="text-dark">${data.berlaku}</p>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i><br>
                                </div>
                              </div>
                            </div>
                            </div>
                            </div>`;
                          
const dataMenu = document.querySelector("#promo");
fetch('api/getpromo')
dataMenu.innerHTML += murah;
});
