var makanan = [ 
  {
    "gambar" : "assets/img/taro.png",
    "nama" : "Taro Milk Tea",
    "harga" : "25.000"
  },
  {
    "gambar" : "assets/img/time.png",
    "nama" : "Chatime Milk Tea",
    "harga" : "23.000"
  },
  {
    "gambar" : "assets/img/berry.png",
    "nama" : "Strawberry Smoothie",
    "harga" : "21.000"
  },
  {
    "gambar" : "assets/img/thai.png",
    "nama" : "Thai tea Original",
    "harga" : "24.000"
  },
  {
    "gambar" : "assets/img/apple.png",
    "nama" : "Apple Green Tea",
    "harga" : "23.000"
  },
  {
    "gambar" : "assets/img/sugar.png",
    "nama" : "Brown Sugar Classic Tea Latte",
    "harga" : "25.000"
  },
  {
    "gambar" : "assets/img/fresh.png",
    "nama" : "Grass Jelly with Fresh Milk",
    "harga" : "21.000"
  },
  {
    "gambar" : "assets/img/mango.png",
    "nama" : "Mango Green Milk Tea",
    "harga" : "23.000"
  },
  {
    "gambar" : "assets/img/hazelnut.png",
    "nama" : "Hazelnut Chocolate Milk Tea",
    "harga" : "25000" 
  }
];

const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');
    fetch('api/getpoppuler')

    elmnt.innerHTML += ` <div class="container" style="margin-right:1px;">
                        <div class="row" style="float: left;">
                        <div class="col-sm m-2" >
                        <div class="card m-3" style="width: 19rem;">
                        <img src="${item.gambar}" class="card-img-top" alt="...">
                        <div class="card-body bg-light">
                          <h5 class="card-title text-dark">${item.nama}</h5>
                          <p class="text-dark">Rp.${item.harga}</p>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i><br>
                          <a href="#" class="btn btn-primary">Beli Sekarang</a>
                        </div>
                      </div>
                      </div>
                      </div>
                      </div>`

}

    makanan.map(callbackMap);
    jumlahMENU(makanan);
                    
  const buttonElmnt = document.querySelector('.button-cari');
                    
  buttonElmnt.addEventListener('click', () => {
                    
  const hasilPencarian = makanan.filter((item, index) => {
                        const inputElmnt = document.querySelector('.input-keyword');
                        const namaItem = item.nama.toLowerCase();
                        const keyword = inputElmnt.value.toLowerCase();
                    
                return namaItem.includes(keyword);
  })
                    
  document.querySelector('#search').innerHTML ='';
  hasilPencarian.map(callbackMap);
});