<br><footer class="bg-warning text-black p-4 w-100" style="display:inline-flex;">
        <div class="row">
          <div class="col-md-3 text-dark">
            <h5>HUBUNGI KAMI</h5>
            <ul>
              <li> 0819-0987-4205 </li>
              <li>chatimekuu@gmail.com</li>
            </ul>
          </div>
          <div class="col-md-3 text-dark">
            <h5>TENTANG KAMI</h5>
            <p>Chatime adalah toko teh dan kopi yang sangat populer di Indonesia. menunya dipenuhi dengan minuman lezat untuk Anda nikmati.</p>
          </div>
          <div class="col-md-3 text-dark">
            <h5>DEVELOPED BY Chatime</h5>
            <ul>
              <li>Copyright <i class="far fa-copyright"></i> 2020</li>
            </ul>
          </div>
        </div>
      </footer>