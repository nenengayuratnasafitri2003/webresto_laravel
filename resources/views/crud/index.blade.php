<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">

    <title>CRUD</title>
  </head>
  <body style="background-color: white;">

    @include('layouts.header');
<body>

      
<div class="container-admin" style="margin: 10px">
    @if(session('success'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Succes!</strong> {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

        <table class="table table-hover table-striped">
            <thead>
                <tr class="bg-dark text-light">
                    <th>GAMBAR</th>
                    <th>NAME</th>
                    <th>Harga</th>
                    <th style="padding-left: 50px; padding-right: 50px; text-align: left;"></th>
                </tr>
            </thead>

            @foreach ($menus as $data)
            <tr>
                <td>{{$data->gambar}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->harga}}</td>
                <td>{{$data->desc}}</td>               
                <td>
                    <a href="/menus/{{$data->id}}/edit" class="btn btn-dark btn-sm text-light">EDIT</a>
                    <a href="/menus/{{$data->id}}/delete" class="btn btn-primary btn-sm" onclick="return confirm('Apakah Kamu Yakin Data Akan Dihapus?')">DELETE</a>
                </td>
            </tr>
            @endforeach

        </table>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-light"  style="background: pink;">
            <div class="modal-header" style="border-bottom: 2px solid white;">
                <h5 class="modal-title" id="exampleModalLabel">ADD MENU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- Popup Forms --}}
                <form action="/menus/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input style="border:none" name="gambar" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="GAMBAR" value="assets/">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Menu</label>
                        <input style="border:none" name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NAMA MENU">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Harga</label>
                        <input style="border:none" name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="HARGA">
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 2px solid white;">
                    <button type="button" class="btn btn-danger text-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark text-light">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>


